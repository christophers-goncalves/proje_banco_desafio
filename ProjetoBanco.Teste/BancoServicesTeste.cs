using Moq;
using projetoBanco.Domain.Model;
using projetoBanco.Domain.Services;
using projetoBanco.Infrastructure.Data.Repository;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ProjetoBanco.Teste
{
    public class BancoServicesTeste
    {

       


        [Fact]
        public async void TesteNovoUser()
        {
            var Result = new BancoServices();
            await Result.NovoUser( new PessoaComun {NomeCompleto="lacost",  CnpjCpf=11223344 , Email="teste123@gmail.com",Saldo=0});
            return Result;
        }
    }
}
