﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using projetoBanco.Domain.Model;
using projetoBanco.Domain.Services;
using projetoBanco.Infrastructure.Data.Repository;
using System.Threading.Tasks;

namespace projetoBanco.Controllers
{

    [Route("api/[controller]")]
    [ApiController]


    public class LojistaController : ControllerBase /////// controler dos lojistas
    {
        private readonly BancoServices _Bservices;

        public LojistaController(BancoServices bservices) // injeção de dependencia
        {
            _Bservices = bservices;
        }



        [HttpGet("ListarLojista")]
        public async Task<IActionResult> ListandoLojista() //// retorna todos os usuarios lojistas
        {
            var list = await _Bservices.ListLuserLojista();

            return Ok(list);
        }



        [HttpGet("Lojista/{id}")]
        public async Task<ActionResult> BuscandoLojistaPeloId(int id) // buscar por id 
        {
            try
            {
                var iten = await _Bservices.LojistaById(id);
                return Ok(iten);

            }
            catch (System.Exception ex)
            {

                return BadRequest("=== deu ruim === " + ex);
            }
        }



        [HttpPost("cadastroLojista")]
        public async Task<ActionResult> NovoCadastro(Lojista model) ///cadastra usuario 
        {
            try
            {
                if (model.Id != 0)
                {
                    return BadRequest("não é necessario inserir o id no cadastro o sistema jã adiciona automaticamente");
                }
                else
                {
                    if (model.Saldo != 0)
                    {
                        return BadRequest("Não é possiveladicionar saldo durante o cadastro");
                    }
                    else
                    {
                        var itemEmail = await _Bservices.ValidaçãoEmail(model.Email);
                        var itemCpf = await _Bservices.ValidaçãoCpf(model.CnpjCpf);

                        if ((itemCpf == true) || (itemEmail == true))
                        {
                            return BadRequest(" EMAIL OU CPF FORNECIDOS EM USO");

                        }
                        else
                        {
                            await _Bservices.NovoUserLojista(model);
                            return Ok("CADASTRADO COM SUCESSO");
                        }
                    }

                }
            }
            catch (System.Exception ex)
            {
                return BadRequest("deu erro" + ex);
            }
        } //cadastro de LOjista



        [HttpPut("{id}")]
        public async Task<int> Atualizar(Lojista model) /// atualiza Lojista
        {
            return await _Bservices.AtttUserLojista(model);
        }




        [HttpDelete("deleteUser/{id}")]
        public async Task<bool> Delete(int id)
        {
            await _Bservices.DelUserLojista(id);

            return true;
        }   // deletar lojista
    }
}
