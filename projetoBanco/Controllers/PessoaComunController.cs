﻿using Microsoft.AspNetCore.Mvc;
using projetoBanco.Domain.Model;
using projetoBanco.Domain.Services;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace projetoBanco.Controllers

{

    [Route("api/PesoaComum")]
    [ApiController]


    public class PessoaComunController : ControllerBase
    {

        private readonly BancoServices _Repo;



        public PessoaComunController(BancoServices repo)
        {
            _Repo = repo;
        }




        // GET: api/<BancoController>
        [HttpGet("listusuario")]
        public async Task<ActionResult> ListandoUsiarios()
        {
            var list = await _Repo.Listusuario();

            return Ok(list);
        }

        [HttpGet("model")]
        public ActionResult Get()
        {
            var trans = new Transferencia();
            return Ok(trans);
        }

        //// GET api/<BancoController>/5
        [HttpGet]
        public async Task<ActionResult> BuscandoUsuarioPeloId(int id)
        {
            try
            {
                var res = await _Repo.Retorno(id);
                return Ok(res);
            }
            catch (System.Exception ex)
            {

                return BadRequest("=== deu ruim === " + ex);
            }
        }




        ////// POST api/<BancoController>
        [HttpPost("cadastro")]
        public async Task<IActionResult> NovoCadastro(PessoaComun model) ///cadastra usuario 
        {
            try
            {
                // validação de entrada 
                if (model.Id != 0) // validação de id
                {
                    return BadRequest("não é necessario inserir o id no cadastro o sistema jã adiciona automaticamente");
                }
                else
                {
                    if (model.Saldo != 0) // validação de entrada de saldo
                    {
                        return BadRequest("Não é possiveladicionar saldo durante o cadastro");
                    }
                    else
                    {

                        var itemEmail = await _Repo.ComumByemail(model.Email);
                        var itemCpf = await _Repo.ComumByCpf(model.CnpjCpf);
                        if ((itemEmail != null) || (itemCpf != null))
                        {
                            return BadRequest(" EMAIL OU CPF FORNECIDOS EM USO");
                        }
                        else
                        {
                            await _Repo.NovoUser(model);
                            return Ok("CADASTRADO COM SUCESSO");
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                return BadRequest("vish deu erro" + ex);
            }
        }



        // PUT api/<BancoController>/5
        [HttpPut("atualizarUser/{id}")]
        public async Task<int> Atualizar(PessoaComun model) /// atualiza usuario
        {
            return await _Repo.AtttUserComum(model);
        }


        [HttpPut("atualizarsaldo/{id}")]
        //public async Task<float> Atualizarsaldo(PessoaComun model) /// atualiza usuario
        // {
        //    var item = model.Saldo;
        //     return await _Repo.Atttsaldoteste(item);
        // }




        // DELETE api/<BancoController>/5
        [HttpDelete("deleteUser/{id}")]
        public async Task<bool> Delete(int id)
        {
            await _Repo.DelUserPComun(id);
            return true;
        }
    }
}