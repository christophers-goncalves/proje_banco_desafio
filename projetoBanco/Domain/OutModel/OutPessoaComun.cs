﻿namespace projetoBanco.Domain.OutModel
{
    public class OutPessoaComun
    {

        public OutPessoaComun()
        {

        }
        public int Id { get; set; }
        public string NomeCompleto { get; set; }
        //public int CnpjCpf { get; set; }
        public string Email { get; set; }
        // private string Senha { get; set; }
        public float Saldo { get; set; }
    }
}
