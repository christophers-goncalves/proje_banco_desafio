﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace projetoBanco.Migrations
{
    public partial class inittranst : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TranferenciasRelaçao");

            migrationBuilder.AddColumn<int>(
                name: "PessoaComunId",
                table: "Transferencias",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transferencias_PessoaComunId",
                table: "Transferencias",
                column: "PessoaComunId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaComunId",
                table: "Transferencias",
                column: "PessoaComunId",
                principalTable: "Pessoas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaComunId",
                table: "Transferencias");

            migrationBuilder.DropIndex(
                name: "IX_Transferencias_PessoaComunId",
                table: "Transferencias");

            migrationBuilder.DropColumn(
                name: "PessoaComunId",
                table: "Transferencias");

            migrationBuilder.CreateTable(
                name: "TranferenciasRelaçao",
                columns: table => new
                {
                    PcomunId = table.Column<int>(type: "int", nullable: false),
                    TransferenciaId = table.Column<int>(type: "int", nullable: false),
                    PessoaId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranferenciasRelaçao", x => new { x.PcomunId, x.TransferenciaId });
                    table.ForeignKey(
                        name: "FK_TranferenciasRelaçao_Pessoas_PessoaId",
                        column: x => x.PessoaId,
                        principalTable: "Pessoas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TranferenciasRelaçao_Transferencias_TransferenciaId",
                        column: x => x.TransferenciaId,
                        principalTable: "Transferencias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                })
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.CreateIndex(
                name: "IX_TranferenciasRelaçao_PessoaId",
                table: "TranferenciasRelaçao",
                column: "PessoaId");

            migrationBuilder.CreateIndex(
                name: "IX_TranferenciasRelaçao_TransferenciaId",
                table: "TranferenciasRelaçao",
                column: "TransferenciaId");
        }
    }
}
