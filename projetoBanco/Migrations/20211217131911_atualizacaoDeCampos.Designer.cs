﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using projetoBanco.Domain.Model;

namespace projetoBanco.Migrations
{
    [DbContext(typeof(BancoContext))]
    [Migration("20211217131911_atualizacaoDeCampos")]
    partial class atualizacaoDeCampos
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 64)
                .HasAnnotation("ProductVersion", "5.0.12");

            modelBuilder.Entity("projetoBanco.Domain.Model.Lojista", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("CnpjCpf")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .HasColumnType("longtext");

                    b.Property<string>("NomeCompleto")
                        .HasColumnType("longtext");

                    b.Property<float>("Saldo")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("Lojistas");
                });

            modelBuilder.Entity("projetoBanco.Domain.Model.PessoaComun", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<int>("CnpjCpf")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .HasColumnType("longtext");

                    b.Property<string>("NomeCompleto")
                        .HasColumnType("longtext");

                    b.Property<float>("Saldo")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("Pessoas");
                });

            modelBuilder.Entity("projetoBanco.Domain.Model.Transferencia", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .HasColumnType("longtext");

                    b.Property<string>("PCemail")
                        .HasColumnType("longtext");

                    b.Property<int?>("PessoaComunId")
                        .HasColumnType("int");

                    b.Property<float>("Valor")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.HasIndex("PessoaComunId");

                    b.ToTable("Transferencias");
                });

            modelBuilder.Entity("projetoBanco.Domain.Model.Transferencia", b =>
                {
                    b.HasOne("projetoBanco.Domain.Model.PessoaComun", null)
                        .WithMany("Transferencias")
                        .HasForeignKey("PessoaComunId");
                });

            modelBuilder.Entity("projetoBanco.Domain.Model.PessoaComun", b =>
                {
                    b.Navigation("Transferencias");
                });
#pragma warning restore 612, 618
        }
    }
}
