﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace projetoBanco.Migrations
{
    public partial class atualizacao3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaId",
                table: "Transferencias");

            migrationBuilder.RenameColumn(
                name: "PessoaId",
                table: "Transferencias",
                newName: "PessoaComunId");

            migrationBuilder.RenameColumn(
                name: "PCemail",
                table: "Transferencias",
                newName: "emailDe");

            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Transferencias",
                newName: "EmailPara");

            migrationBuilder.RenameIndex(
                name: "IX_Transferencias_PessoaId",
                table: "Transferencias",
                newName: "IX_Transferencias_PessoaComunId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaComunId",
                table: "Transferencias",
                column: "PessoaComunId",
                principalTable: "Pessoas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaComunId",
                table: "Transferencias");

            migrationBuilder.RenameColumn(
                name: "emailDe",
                table: "Transferencias",
                newName: "PCemail");

            migrationBuilder.RenameColumn(
                name: "PessoaComunId",
                table: "Transferencias",
                newName: "PessoaId");

            migrationBuilder.RenameColumn(
                name: "EmailPara",
                table: "Transferencias",
                newName: "Email");

            migrationBuilder.RenameIndex(
                name: "IX_Transferencias_PessoaComunId",
                table: "Transferencias",
                newName: "IX_Transferencias_PessoaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transferencias_Pessoas_PessoaId",
                table: "Transferencias",
                column: "PessoaId",
                principalTable: "Pessoas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
